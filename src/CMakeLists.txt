cmake_minimum_required(VERSION 3.13)

set(SYSDIR ${CMAKE_CURRENT_LIST_DIR}/system)
set(APPDIR ${CMAKE_CURRENT_LIST_DIR}/app)


target_sources(${FILE_ELF} 
    PRIVATE
        ${SYSDIR}/clock_config.c ${SYSDIR}/clock_config.h
        ${SYSDIR}/pin_mux.c ${SYSDIR}/pin_mux.h
        ${SYSDIR}/peripherals.c ${SYSDIR}/peripherals.h
        ${SYSDIR}/peripherals_other.c ${SYSDIR}/peripherals_other.h
        ${APPDIR}/main.cc
        ${APPDIR}/accelerometer_regs.h
        ${APPDIR}/accelerometer.cc ${APPDIR}/accelerometer.h
)

target_include_directories(${FILE_ELF}
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/system
        ${CMAKE_CURRENT_LIST_DIR}/app
)

