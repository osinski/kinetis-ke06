/*
 * accelerometer.h
 * Copyright (C) 2020 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef ACCELEROMETER_H
#define ACCELEROMETER_H

#include "stdint.h"
#include "fsl_i2c.h"

#include "accelerometer_regs.h"

class MMA8541 {
    public:
        MMA8541() = delete;
        MMA8541(I2C_Type *i2ctype);

        ~MMA8541() = default;

        uint8_t getX() const {return x;}
        uint8_t getY() const {return y;}
        uint8_t getZ() const {return z;}

        void read();
        void setup();

    private:
        I2C_Type *i2c_interface;
        i2c_master_config_t i2c_config;
        i2c_master_handle_t i2c_handle;
        i2c_master_transfer_t i2c_transfer;

        const uint8_t i2cAddress = 0x1D;

        uint8_t whoami;
        volatile uint8_t x;
        volatile uint8_t y;
        volatile uint8_t z;

        uint8_t readReg(uint8_t addr);
        void readBurst(uint8_t startAddr, uint8_t readSize, uint8_t *data);
        void writeReg(uint8_t addr, uint8_t val);
        void writeBurst(uint8_t startAddr, uint8_t writeSize, uint8_t *data);
};

#endif /* !ACCELEROMETER_H */
