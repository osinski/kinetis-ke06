/*
 * accelerometer_regs.h
 * Copyright (C) 2020 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef ACCELEROMETER_REGS_H
#define ACCELEROMETER_REGS_H

#include "stdint.h"

namespace MMA8541_REGISTER {
    namespace STATUS {
        constexpr uint8_t addr = 0x00;

        namespace BITS {
            namespace REALTIME {
                constexpr uint8_t ZYXOW = (1 << 7);
                constexpr uint8_t ZOW = (1 << 6);
                constexpr uint8_t YOW = (1 << 5);
                constexpr uint8_t XOW = (1 << 4);
                constexpr uint8_t ZYXDR = (1 << 3);
                constexpr uint8_t ZDR = (1 << 2);
                constexpr uint8_t YDR = (1 << 1);
                constexpr uint8_t XDR = (1 << 0);
            }
            namespace FIFO {
                constexpr uint8_t F_OVF = (1 << 7);
                constexpr uint8_t F_WMRK_FLAG = (1 << 6);
                constexpr uint8_t F_CNTS = (1 << 5);
                constexpr uint8_t F_CNT4 = (1 << 4);
                constexpr uint8_t F_CNT3 = (1 << 3);
                constexpr uint8_t F_CNT2 = (1 << 2);
                constexpr uint8_t F_CNT1 = (1 << 1);
                constexpr uint8_t F_CNT0 = (1 << 0);
            }
        }
    }
    namespace OUT_X {
        namespace MSB {
            constexpr uint8_t addr = 0x01;
        }
        namespace LSB {
            constexpr uint8_t addr = 0x02;
        }
    }
    namespace OUT_Y {
        namespace MSB {
            constexpr uint8_t addr = 0x03;
        }
        namespace LSB {
            constexpr uint8_t addr = 0x04;
        }
    }
    namespace OUT_Z {
        namespace MSB {
            constexpr uint8_t addr = 0x05;
        }
        namespace LSB {
            constexpr uint8_t addr = 0x06;
        }
    }
    namespace F_SETUP {
        constexpr uint8_t addr = 0x09;

        namespace BITS {
            constexpr uint8_t F_MODE1 = (1 << 7);
            constexpr uint8_t F_MODE0 = (1 << 6);
            constexpr uint8_t F_WMRK5 = (1 << 5);
            constexpr uint8_t F_WMRK4 = (1 << 4);
            constexpr uint8_t F_WMRK3 = (1 << 3);
            constexpr uint8_t F_WMRK2 = (1 << 2);
            constexpr uint8_t F_WMRK1 = (1 << 1);
            constexpr uint8_t F_WMRK0 = (1 << 0);
        }
    }
    namespace TRIG_CFG {
        constexpr uint8_t addr = 0x0A;

        namespace BITS {
            constexpr uint8_t Trig_TRANS = (1 << 5);
            constexpr uint8_t Trig_LNDPRT = (1 << 4);
            constexpr uint8_t Trig_PULSE = (1 << 3);
            constexpr uint8_t Trig_FF_MT = (1 << 2);
        }
    }
    namespace SYSMOD {
        constexpr uint8_t addr = 0x0B;

        namespace BITS {
            constexpr uint8_t FGERR = (1 << 7);
            constexpr uint8_t FGT_4 = (1 << 6);
            constexpr uint8_t FGT_3 = (1 << 5);
            constexpr uint8_t FGT_2 = (1 << 4);
            constexpr uint8_t FGT_1 = (1 << 3);
            constexpr uint8_t FGT_0 = (1 << 2);
            constexpr uint8_t SYSMOD1 = (1 << 1);
            constexpr uint8_t SYSMOD0 = (1 << 0);
        }
    }
    namespace INT_SOURCE {
        constexpr uint8_t addr = 0x0C;

        namespace BITS {
            constexpr uint8_t SRC_ASLP = (1 << 7);
            constexpr uint8_t SRC_FIFO = (1 << 6);
            constexpr uint8_t SRC_TRANS = (1 << 5);
            constexpr uint8_t SRC_LNDPRT = (1 << 4);
            constexpr uint8_t SRC_PULSE = (1 << 3);
            constexpr uint8_t SRC_FF_MT = (1 << 2);
            constexpr uint8_t SRC_DRDY = (1 << 0);
        }
    }
    namespace WHO_AM_I {
        constexpr uint8_t addr = 0x0D;
    }
    namespace XYZ_DATA_CFG {
        constexpr uint8_t addr = 0x0E;

        namespace BITS {
            constexpr uint8_t HPF_OUT = (1 << 4);
            constexpr uint8_t FS1 = (1 << 2);
            constexpr uint8_t FS0 = (1 << 0);
        }
    }
    namespace HP_FILTER_CUTOFF {
        constexpr uint8_t addr = 0x0F;

        namespace BITS {
            constexpr uint8_t Pulse_HPF_BYP = (1 << 5);
            constexpr uint8_t Pulse_LPF_EN = (1 << 4);
            constexpr uint8_t SEL1 = (1 << 1);
            constexpr uint8_t SEL0 = (1 << 0);
        }
    }
    namespace PL_STATUS {
        constexpr uint8_t addr = 0x10;

        namespace BITS {
            constexpr uint8_t NEWLP = (1 << 7);
            constexpr uint8_t LO = (1 << 6);
            constexpr uint8_t LAPO1 = (1 << 2);
            constexpr uint8_t LAPO0 = (1 << 1);
            constexpr uint8_t BAFRO = (1 << 0);
        }
    }
    namespace PL_CFG {
        constexpr uint8_t addr = 0x11;

        namespace BITS {
            constexpr uint8_t DBCNTM = (1 << 7);
            constexpr uint8_t PL_EN = (1 << 6);
        }
    }
    namespace PL_COUNT {
        constexpr uint8_t addr = 0x12;
    }
    namespace PL_BF_ZCOMP {
        constexpr uint8_t addr = 0x13;

        namespace BITS {
            constexpr uint8_t BKFR1 = (1 << 7);
            constexpr uint8_t BKFR0 = (1 << 6);
            constexpr uint8_t ZLOCK2 = (1 << 2);
            constexpr uint8_t ZLOCK1 = (1 << 1);
            constexpr uint8_t ZLOCK0 = (1 << 0);
        }
    }
    namespace P_L_THS_REG {
        constexpr uint8_t addr = 0x14;

        namespace BITS {
            constexpr uint8_t PL_THS4 = (1 << 7);
            constexpr uint8_t PL_THS3 = (1 << 6);
            constexpr uint8_t PL_THS2 = (1 << 5);
            constexpr uint8_t PL_THS1 = (1 << 4);
            constexpr uint8_t PL_THS0 = (1 << 3);
            constexpr uint8_t HYS2 = (1 << 2);
            constexpr uint8_t HYS1 = (1 << 1);
            constexpr uint8_t HYS0 = (1 << 0);
        }
    }
    namespace FF_MT_CFG {
        constexpr uint8_t addr = 0x15;

        namespace BITS {
            constexpr uint8_t ELE = (1 << 7);
            constexpr uint8_t OAE = (1 << 6);
            constexpr uint8_t ZEFE = (1 << 5);
            constexpr uint8_t YEFE = (1 << 4);
            constexpr uint8_t XEFE = (1 << 3);
        }
    }
    namespace FF_MT_SRC {
        constexpr uint8_t addr = 0x16;

        namespace BITS {
            constexpr uint8_t EA = (1 << 7);
            constexpr uint8_t ZHE = (1 << 5);
            constexpr uint8_t ZHP = (1 << 4);
            constexpr uint8_t YHE = (1 << 3);
            constexpr uint8_t YHP = (1 << 2);
            constexpr uint8_t XHE = (1 << 1);
            constexpr uint8_t XHP = (1 << 0);
        }
    }
    namespace FF_MT_THS {
        constexpr uint8_t addr = 0x17;

        namespace BITS {
            constexpr uint8_t DBCNTM = (1 << 7);
            constexpr uint8_t THS6 = (1 << 6);
            constexpr uint8_t THS5 = (1 << 5);
            constexpr uint8_t THS4 = (1 << 4);
            constexpr uint8_t THS3 = (1 << 3);
            constexpr uint8_t THS2 = (1 << 2);
            constexpr uint8_t THS1 = (1 << 1);
            constexpr uint8_t THS0 = (1 << 0);
        }
    }
    namespace FF_MT_COUNT {
        constexpr uint8_t addr = 0x18;
    }
    namespace TRANSIENT_CFG {
        constexpr uint8_t addr = 0x1D;

        namespace BITS {
            constexpr uint8_t ELE = (1 << 4);
            constexpr uint8_t ZTEFE = (1 << 3);
            constexpr uint8_t YTEFE = (1 << 2);
            constexpr uint8_t XTEFE = (1 << 1);
            constexpr uint8_t HPF_BYP = (1 << 0);
        }
    }
    namespace TRANSIENT_SCR {
        constexpr uint8_t addr = 0x1E;

        namespace BITS {
            constexpr uint8_t EA = (1 << 6);
            constexpr uint8_t ZTRANSE = (1 << 5);
            constexpr uint8_t Z_Trans_Pol = (1 << 4);
            constexpr uint8_t YTRANSE = (1 << 3);
            constexpr uint8_t Y_Trans_Pol = (1 << 2);
            constexpr uint8_t XTRANSE = (1 << 1);
            constexpr uint8_t X_Trans_Pol = (1 << 0);
        }
    }
    namespace TRANSIENT_THS {
        constexpr uint8_t addr = 0x1F;

        namespace BITS {
            constexpr uint8_t DBCNTM = (1 << 7);
            constexpr uint8_t THS6 = (1 << 6);
            constexpr uint8_t THS5 = (1 << 5);
            constexpr uint8_t THS4 = (1 << 4);
            constexpr uint8_t THS3 = (1 << 3);
            constexpr uint8_t THS2 = (1 << 2);
            constexpr uint8_t THS1 = (1 << 1);
            constexpr uint8_t THS0 = (1 << 0);
        }
    }
    namespace TRANSIENT_COUNT {
        constexpr uint8_t addr = 0x20;
    }
    namespace PULSE_CFG {
        constexpr uint8_t addr = 0x21;

        namespace BITS {
            constexpr uint8_t DPA = (1 << 7);
            constexpr uint8_t ELE = (1 << 6);
            constexpr uint8_t ZDPEFE = (1 << 5);
            constexpr uint8_t ZSPEFE = (1 << 4);
            constexpr uint8_t YDPEFE = (1 << 3);
            constexpr uint8_t YSPEFE = (1 << 2);
            constexpr uint8_t XDPEFE = (1 << 1);
            constexpr uint8_t XSPEFE = (1 << 0);
        }
    }
    namespace PULSE_SRC {
        constexpr uint8_t addr = 0x22;

        namespace BITS {
            constexpr uint8_t EA = (1 << 7);
            constexpr uint8_t AxZ = (1 << 6);
            constexpr uint8_t AxY = (1 << 5);
            constexpr uint8_t AxX = (1 << 4);
            constexpr uint8_t DPE = (1 << 3);
            constexpr uint8_t PolZ = (1 << 2);
            constexpr uint8_t PolY = (1 << 1);
            constexpr uint8_t PolX = (1 << 0);
        }
    }
    namespace PULSE_THSX {
        constexpr uint8_t addr = 0x23;
    }
    namespace PULSE_THSY {
        constexpr uint8_t addr = 0x24;
    }
    namespace PULSE_THSZ {
        constexpr uint8_t addr = 0x25;
    }
    namespace PULSE_TMLT {
        constexpr uint8_t addr = 0x26;
    }
    namespace PULSE_LTCY {
        constexpr uint8_t addr = 0x27;
    }
    namespace PULSE_WIND {
        constexpr uint8_t addr = 0x28;
    }
    namespace ASLP_COUNT {
        constexpr uint8_t addr = 0x29;
    }
    namespace CTRL_REG1 {
        constexpr uint8_t addr = 0x2A;

        namespace BITS {
            constexpr uint8_t ASLP_RATE1 = (1 << 7);
            constexpr uint8_t ASLP_RATE0 = (1 << 6);
            constexpr uint8_t DR2 = (1 << 5);
            constexpr uint8_t DR1 = (1 << 4);
            constexpr uint8_t DR0 = (1 << 3);
            constexpr uint8_t LNOISE = (1 << 2);
            constexpr uint8_t F_READ = (1 << 1);
            constexpr uint8_t ACTIVE = (1 << 0);
        }
    }
    namespace CTRL_REG2 {
        constexpr uint8_t addr = 0x2B;

        namespace BITS {
            constexpr uint8_t ST = (1 << 7);
            constexpr uint8_t RST = (1 << 6);
            constexpr uint8_t SMODS1 = (1 << 4);
            constexpr uint8_t SMODS0 = (1 << 3);
            constexpr uint8_t SLPE = (1 << 2);
            constexpr uint8_t MODS1 = (1 << 1);
            constexpr uint8_t MODS0 = (1 << 0);
        }
    }
    namespace CTRL_REG3 {
        constexpr uint8_t addr = 0x2C;

        namespace BITS {
            constexpr uint8_t FIFO_GATE = (1 << 7);
            constexpr uint8_t WAKE_TRANS = (1 << 6);
            constexpr uint8_t WAKE_LNDPRT = (1 << 5);
            constexpr uint8_t WAKE_PULSE = (1 << 4);
            constexpr uint8_t WAKE_FF_MT = (1 << 3);
            constexpr uint8_t IPOL = (1 << 1);
            constexpr uint8_t PP_OD = (1 << 0);
        }
    }
    namespace CTRL_REG4 {
        constexpr uint8_t addr = 0x2D;

        namespace BITS {
            constexpr uint8_t INT_EN_ASLP = (1 << 7);
            constexpr uint8_t INT_EN_FIFO = (1 << 6);
            constexpr uint8_t INT_EN_TRANS = (1 << 5);
            constexpr uint8_t INT_EN_LNDPRT = (1 << 4);
            constexpr uint8_t INT_EN_PULSE = (1 << 3);
            constexpr uint8_t INT_EN_FF_MT = (1 << 2);
            constexpr uint8_t INT_EN_DRDY = (1 << 0);
        }
    }
    namespace CTRL_REG5 {
        constexpr uint8_t addr = 0x2E;

        namespace BITS {
            constexpr uint8_t INT_CFG_ASLP = (1 << 7);
            constexpr uint8_t INT_CFG_FIFO = (1 << 6);
            constexpr uint8_t INT_CFG_TRANS = (1 << 5);
            constexpr uint8_t INT_CFG_LNDPRT = (1 << 4);
            constexpr uint8_t INT_CFG_PULSE = (1 << 3);
            constexpr uint8_t INT_CFG_FF_MT = (1 << 2);
            constexpr uint8_t INT_CFG_DRDY = (1 << 0);
        }
    }
    namespace OFF_X {
        constexpr uint8_t addr = 0x2F;
    }
    namespace OFF_Y {
        constexpr uint8_t addr = 0x30;
    }
    namespace OFF_Z {
        constexpr uint8_t addr = 0x31;
    }
}

#endif /* !ACCELEROMETER_REGS_H */
