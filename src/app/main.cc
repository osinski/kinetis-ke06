/*
 * main.cc
 * Copyright (C) 2020 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "stdint.h"

#include "MKE06Z4.h"

#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"

#include "accelerometer.h"

#include "fsl_mscan.h"
#include "fsl_pit.h"

struct IRQ_flags {
    bool pit;
} IRQflags;

auto acc = MMA8541(I2C_ACCEL_PERIPHERAL);

volatile uint32_t updateValX;
volatile uint32_t updateValY;
volatile uint32_t updateValZ;

int main()
{
    BOARD_InitBootClocks();
    BOARD_InitBootPins();
    BOARD_InitPeripherals();


    mscan_frame_t canframe;
    mscan_config_t canconfig;
    MSCAN_GetDefaultConfig(&canconfig);
    canconfig.baudRate = 125000U;
    MSCAN_Init(MSCAN, &canconfig, CLOCK_GetFreq(kCLOCK_Osc0ErClk));

    

    IRQflags.pit = false;

    acc.setup();

    while(true) {

        if (IRQflags.pit) {
            acc.read();

            FTM_StopTimer(FTM_RGBLED_PERIPHERAL);

            updateValX = acc.getX() * 100 / 255;
            FTM_UpdatePwmDutycycle(FTM_RGBLED_PERIPHERAL, kFTM_Chnl_3,
                                    kFTM_EdgeAlignedPwm, (uint8_t)updateValX);

            updateValY = acc.getY() * 100 / 255;
            FTM_UpdatePwmDutycycle(FTM_RGBLED_PERIPHERAL, kFTM_Chnl_4,
                                    kFTM_EdgeAlignedPwm, (uint8_t)updateValY);

            updateValZ = acc.getZ() * 100 / 255;
            FTM_UpdatePwmDutycycle(FTM_RGBLED_PERIPHERAL, kFTM_Chnl_5,
                                    kFTM_EdgeAlignedPwm, (uint8_t)updateValZ);

            FTM_StartTimer(FTM_RGBLED_PERIPHERAL, kFTM_SystemClock);


            canframe.ID_Type.ID = 'c';
            canframe.dataWord0 = (uint32_t)(updateValZ << 16 | updateValY << 8 |
                                            updateValX << 0);
            canframe.format = kMSCAN_FrameFormatStandard;
            canframe.type = kMSCAN_FrameTypeData;
            canframe.DLR = 4;

            MSCAN_TransferSendBlocking(MSCAN, &canframe);

            IRQflags.pit = false;
        }
    }
}



// INTERRUPTS
extern "C" {
    void PIT_CH0_IRQHandler(void)
    {
        // clear interrupt flag
        PIT_ClearStatusFlags(PIT_PERIPHERAL, PIT_CHANNEL_0, kPIT_TimerFlag);

        IRQflags.pit = true;
    }
}
