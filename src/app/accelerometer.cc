/*
 * accelerometer.cc
 * Copyright (C) 2020 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "accelerometer.h"

MMA8541::MMA8541(I2C_Type *i2ctype)
{
    i2c_interface = i2ctype;

    i2c_transfer.slaveAddress = i2cAddress; // 0x1D;
    i2c_transfer.direction = kI2C_Write;
    i2c_transfer.flags = kI2C_TransferDefaultFlag;
    i2c_transfer.data = 0;
    i2c_transfer.dataSize = 0;
    i2c_transfer.subaddress = 0;
    i2c_transfer.subaddressSize = 0;
}

void MMA8541::setup()
{
    writeReg(MMA8541_REGISTER::CTRL_REG1::addr, 0x01);
}

void MMA8541::read()
{
    auto status = readReg(MMA8541_REGISTER::STATUS::addr);
    whoami = readReg(MMA8541_REGISTER::WHO_AM_I::addr);
    x = readReg(MMA8541_REGISTER::OUT_X::MSB::addr);
    y = readReg(MMA8541_REGISTER::OUT_Y::MSB::addr);
    z = readReg(MMA8541_REGISTER::OUT_Z::MSB::addr);
}

uint8_t MMA8541::readReg(uint8_t addr)
{
    uint8_t data = addr;

    i2c_transfer.flags = kI2C_TransferNoStopFlag;
    i2c_transfer.subaddress = 0;
    i2c_transfer.subaddressSize = 0;
    i2c_transfer.data = &data;
    i2c_transfer.dataSize = 1;
    i2c_transfer.direction = kI2C_Write;

    I2C_MasterTransferBlocking(i2c_interface, &i2c_transfer);

    i2c_transfer.flags = kI2C_TransferRepeatedStartFlag;
    i2c_transfer.direction = kI2C_Read;

    I2C_MasterTransferBlocking(i2c_interface, &i2c_transfer);

    return data;
}

void MMA8541::readBurst(uint8_t startAddr, uint8_t readSize,
                            uint8_t *data)
{

    for(uint8_t i = 0; i < readSize; i++) {

    }
}

void MMA8541::writeReg(uint8_t addr, uint8_t val)
{
    i2c_transfer.subaddress = addr;
    i2c_transfer.subaddressSize = 1;
    i2c_transfer.data = &val;
    i2c_transfer.dataSize = 1;

    i2c_transfer.direction = kI2C_Write;

    I2C_MasterTransferBlocking(i2c_interface, &i2c_transfer);
}

void MMA8541::writeBurst(uint8_t startAddr, uint8_t writeSize,
                            uint8_t *data)
{
    for(uint8_t i = 0; i < writeSize; i++) {

    }
}

