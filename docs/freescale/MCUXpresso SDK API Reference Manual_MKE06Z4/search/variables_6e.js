var searchData=
[
  ['nbytes',['nbytes',['../a00016.html#a12372a02e497b34e78291de9718d9686',1,'_i2c_master_edma_handle::nbytes()'],['../a00043.html#a42e9b4098a1a5bc1891aad20e1e36a8a',1,'_uart_edma_handle::nbytes()']]],
  ['next',['next',['../a00020.html#a460f03213c38cd22939b93c36c3d5cd0',1,'list_element_t']]],
  ['nextchanedgemode',['nextChanEdgeMode',['../a00011.html#af64561d78c1d94a7d7106785dce6da7c',1,'ftm_dual_edge_capture_param_t']]],
  ['nointeralalign',['noInteralAlign',['../a00022.html#a306fbd3a3215259f9b380f8e8423f172',1,'mmc_card_t::noInteralAlign()'],['../a00027.html#a089143022f4fcf62251935db4ee7e996',1,'sd_card_t::noInteralAlign()']]],
  ['nointernalalign',['noInternalAlign',['../a00028.html#gaa3a92263c58ded5911a0c0bce688352f',1,'_sdio_card']]],
  ['notifytype',['notifyType',['../a00023.html#a2ca3b1a52e315e072a8ab48fcc1dd62a',1,'notifier_notification_block_t']]]
];
