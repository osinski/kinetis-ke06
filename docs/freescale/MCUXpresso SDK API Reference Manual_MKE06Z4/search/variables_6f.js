var searchData=
[
  ['ocr',['ocr',['../a00022.html#a0b3899f0be098f696fd5bc212d4b597e',1,'mmc_card_t::ocr()'],['../a00027.html#a4dfdb06355545a94755d7b7b8b8bb708',1,'sd_card_t::ocr()'],['../a00028.html#ga493c409455409991a2af4ae08e31b386',1,'_sdio_card::ocr()'],['../a00030.html#a944b85aa51494ed8de4761f7ec0528a4',1,'sdspi_card_t::ocr()']]],
  ['operationvoltage',['operationVoltage',['../a00027.html#aef60a9196a9789f243660bb76f3a0ef2',1,'sd_card_t::operationVoltage()'],['../a00028.html#ga7aecd4677ec222958c8daad36033933d',1,'_sdio_card::operationVoltage()']]],
  ['outdiv1',['outDiv1',['../a00017.html#a82f3b6f30bc011a3914a3ba04726994a',1,'sim_clock_config_t']]],
  ['outdiv2',['outDiv2',['../a00017.html#aecaa3438aeb39ccabf0abda111b07156',1,'sim_clock_config_t']]],
  ['outdiv3',['outDiv3',['../a00017.html#a7df63553722d24e791337a6fbd57b15f',1,'sim_clock_config_t']]],
  ['outputlogic',['outputLogic',['../a00123.html#ga9d37ffd9a2943f10a91095759bd52da5',1,'gpio_pin_config_t']]],
  ['outputmode',['outputMode',['../a00039.html#a4bef3df530a9f4209409ab332c6cf99f',1,'spi_master_config_t']]]
];
