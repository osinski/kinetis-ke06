var searchData=
[
  ['sclstopholdtime_5fns',['sclStopHoldTime_ns',['../a00014.html#ac3675a34ece88c54f329e95edf15fb59',1,'i2c_slave_config_t']]],
  ['scr',['scr',['../a00027.html#ae47527a4d44dfb1754d97149e71d7744',1,'sd_card_t::scr()'],['../a00030.html#a246537655ae68ffd8472df9bf6255b27',1,'sdspi_card_t::scr()']]],
  ['scrstructure',['scrStructure',['../a00021.html#adfaf2aa23edd0ebd26e87387c5288583',1,'sd_scr_t']]],
  ['sdbuswidths',['sdBusWidths',['../a00021.html#a2fd29ab1a86fbeaee6241188bd1e5c12',1,'sd_scr_t']]],
  ['sdioversion',['sdioVersion',['../a00028.html#gac882417e33e2c8995e718f7f076cd570',1,'_sdio_card']]],
  ['sdsecurity',['sdSecurity',['../a00021.html#ad886d948d96ec93254fde00d98d854eb',1,'sd_scr_t']]],
  ['sdspecification',['sdSpecification',['../a00021.html#a836c9b35b28e16292282079e6be9b622',1,'sd_scr_t']]],
  ['sdversion',['sdVersion',['../a00028.html#gac23564e5abb6823da4d5d42e7d22e5df',1,'_sdio_card']]],
  ['second',['second',['../a00026.html#a35ffc6bb746f813d06f2814c6d5d9d83',1,'rtc_datetime_t']]],
  ['sectioncmdaddressaligment',['sectionCmdAddressAligment',['../a00010.html#ace5e7199a69c381ede28bd534efd2707',1,'flash_operation_config_t']]],
  ['sectorcmdaddressaligment',['sectorCmdAddressAligment',['../a00010.html#a3b16324d8c454e26e3184f73b23d278d',1,'flash_operation_config_t']]],
  ['sectorcount',['sectorCount',['../a00021.html#a58931c73584fe4ed36d2b38c42a75171',1,'mmc_extended_csd_t']]],
  ['securemode',['secureMode',['../a00021.html#af63961d10a6213b128ae1226e4d9308e',1,'sd_status_t']]],
  ['setfrequency',['setFrequency',['../a00030.html#ac69133eb56496fa30f5a1de66fd0d70f',1,'sdspi_host_t']]],
  ['size',['size',['../a00020.html#a401f82c9b8b482c7d722032cf7ca6282',1,'list_label_t']]],
  ['slaveaddress',['slaveAddress',['../a00014.html#a05851e26565e2b89bd49ef230cc397be',1,'i2c_slave_config_t::slaveAddress()'],['../a00014.html#a3b9c4ae818b1194955db51de0f67795f',1,'i2c_master_transfer_t::slaveAddress()']]],
  ['sleepawaketimeout',['sleepAwakeTimeout',['../a00021.html#ad3dbceb5fa35fd460ef8c59c74cbaf07',1,'mmc_extended_csd_t']]],
  ['sleepcurrentvcc',['sleepCurrentVCC',['../a00021.html#a4947abaa0c48dee6c8432af02e02e031',1,'mmc_extended_csd_t']]],
  ['sleepcurrentvccq',['sleepCurrentVCCQ',['../a00021.html#af91f93080c2af72681bb6a38ca9470cc',1,'mmc_extended_csd_t']]],
  ['speedclass',['speedClass',['../a00021.html#a897c49cd598ad25e03b9a04f9cc1d199',1,'sd_status_t']]],
  ['srcclk',['srcclk',['../a00044.html#aaa9ea3cb62d50a49b907b1baddbeeaa0',1,'uart_rtos_config_t']]],
  ['srcclock_5fhz',['srcClock_Hz',['../a00013.html#a4c820b7ed94ec22cd864fb6074a71ca9',1,'hal_uart_config_t']]],
  ['stat',['stat',['../a00027.html#ac9b7cf861f3224f3a8f14d9b7c52ca1b',1,'sd_card_t']]],
  ['state',['state',['../a00014.html#add7ec18bc8239c5c87ffcec2fbcf5dd8',1,'_i2c_master_handle::state()'],['../a00015.html#acb92e84abaa14c90df432c0a9e2584d4',1,'_i2c_master_dma_handle::state()'],['../a00016.html#a1881cce73748c6ec2865369ddfc7267a',1,'_i2c_master_edma_handle::state()'],['../a00039.html#ae7933252a37be998d127217f34f6fd16',1,'_spi_master_handle::state()'],['../a00038.html#ae11863ad60c9260044f4d07475612497',1,'_spi_dma_handle::state()']]],
  ['stopbitcount',['stopBitCount',['../a00041.html#adf6e33c13910e9ec7c2688f83a462be0',1,'uart_config_t::stopBitCount()'],['../a00033.html#a95a9a17e85a994ec7ce086c72bc6a595',1,'serial_port_uart_config_t::stopBitCount()'],['../a00013.html#a9a261c83422f412e90fb633bd48df6ed',1,'hal_uart_config_t::stopBitCount()']]],
  ['stopbits',['stopbits',['../a00044.html#a2afb208100058edfc05aa161e555483f',1,'uart_rtos_config_t']]],
  ['subaddress',['subaddress',['../a00014.html#ae7facb612714785d4e143e57d47a5af3',1,'i2c_master_transfer_t']]],
  ['subaddresssize',['subaddressSize',['../a00014.html#a9c08797f65f0faac78f44ac038c45c38',1,'i2c_master_transfer_t']]],
  ['supportedcommandset',['supportedCommandSet',['../a00021.html#a4fe0c0ca05ea788af160261e08a70dae',1,'mmc_extended_csd_t']]],
  ['systemspecificationversion',['systemSpecificationVersion',['../a00021.html#abf942a027033d294d03681d658bf38c5',1,'mmc_csd_t']]]
];
