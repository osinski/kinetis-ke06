var searchData=
[
  ['accessmode',['accessMode',['../a00021.html#a41dbbef50527313937ca65fa9a09ea1b',1,'mmc_extended_csd_config_t']]],
  ['accesssize',['accessSize',['../a00021.html#af2bc16d4d2c93c19c92c3791ba61a7d7',1,'mmc_extended_csd_t']]],
  ['activeblocksize',['activeBlockSize',['../a00010.html#a6b4b0904f376a3518805d0dd51356fe7',1,'flash_operation_config_t']]],
  ['activefunctioncount',['activeFunctionCount',['../a00010.html#a219f882d6490567a156d8e03f38807a6',1,'flash_execute_in_ram_function_config_t']]],
  ['activesectorsize',['activeSectorSize',['../a00010.html#acd7eaf8c4af1e02f2b343cbfb67899b9',1,'flash_operation_config_t']]],
  ['addressingmode',['addressingMode',['../a00014.html#a12ca3a31d9a679b8f227d8cf5a470848',1,'i2c_slave_config_t']]],
  ['applicationid',['applicationID',['../a00021.html#a18358f467b8c778a8e7e0ee9df73ef6c',1,'sd_cid_t::applicationID()'],['../a00021.html#a325f07d6b48a1f3eae5bf2332ad24c4a',1,'mmc_cid_t::applicationID()']]],
  ['ausize',['auSize',['../a00021.html#a535a43e71acdebb2115411cc8f388043',1,'sd_status_t']]]
];
