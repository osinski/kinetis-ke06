var searchData=
[
  ['secure_20digital_20card_2fembedded_20multimedia_20card_2fsdio_20card',['Secure Digital Card/Embedded MultiMedia Card/SDIO card',['../a00021.html',1,'']]],
  ['sd_20card_20driver',['SD Card Driver',['../a00027.html',1,'']]],
  ['sdio_20card_20driver',['SDIO Card Driver',['../a00028.html',1,'']]],
  ['spi_20based_20secure_20digital_20card_20_28sdspi_29',['SPI based Secure Digital Card (SDSPI)',['../a00030.html',1,'']]],
  ['semihosting',['Semihosting',['../a00132.html',1,'']]],
  ['serial_20port_20swo',['Serial Port SWO',['../a00032.html',1,'']]],
  ['serial_20port_20uart',['Serial Port Uart',['../a00033.html',1,'']]],
  ['serial_20port_20usb',['Serial Port USB',['../a00034.html',1,'']]],
  ['serial_20port_20virtual_20usb',['Serial Port Virtual USB',['../a00035.html',1,'']]],
  ['serial_20manager',['Serial Manager',['../a00031.html',1,'']]],
  ['shell',['Shell',['../a00036.html',1,'']]],
  ['sim_3a_20system_20integration_20module_20driver',['SIM: System Integration Module Driver',['../a00037.html',1,'']]],
  ['spi_3a_20serial_20peripheral_20interface_20driver',['SPI: Serial Peripheral Interface Driver',['../a00127.html',1,'']]],
  ['spi_20dma_20driver',['SPI DMA Driver',['../a00038.html',1,'']]],
  ['spi_20driver',['SPI Driver',['../a00039.html',1,'']]],
  ['spi_20freertos_20driver',['SPI FreeRTOS driver',['../a00128.html',1,'']]],
  ['swo',['SWO',['../a00133.html',1,'']]]
];
