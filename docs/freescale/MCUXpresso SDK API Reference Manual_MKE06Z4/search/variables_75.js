var searchData=
[
  ['uhsausize',['uhsAuSize',['../a00021.html#a2c4de6b50d23ee9b48d9d150fcc16fcd',1,'sd_status_t']]],
  ['uhsspeedgrade',['uhsSpeedGrade',['../a00021.html#a6cc825664732275578762e431ab7d90e',1,'sd_status_t']]],
  ['upperaddress',['upperAddress',['../a00014.html#af0e8e477e86ac86aaff8cbff61c2dbfc',1,'i2c_slave_config_t']]],
  ['usefaultfilter',['useFaultFilter',['../a00011.html#affb0f68ace5ee1593ca4b83d4fc5a838',1,'ftm_fault_param_t']]],
  ['useglobaltimebase',['useGlobalTimeBase',['../a00011.html#a13ed336a2649044aec62875c26e8b9e7',1,'ftm_config_t']]],
  ['userdata',['userData',['../a00014.html#aad7df570c53adb2e80acd2ba0d39d109',1,'_i2c_master_handle::userData()'],['../a00014.html#a98ea5e99278b386e2ddb99d45a9750ee',1,'_i2c_slave_handle::userData()'],['../a00015.html#a9d8fa45695a25b132f465762176f7c88',1,'_i2c_master_dma_handle::userData()'],['../a00016.html#aa190dc1f8bbea43a9ad7f5f0b6de9859',1,'_i2c_master_edma_handle::userData()'],['../a00039.html#ab8d01b85149d749ab1c748bb5116b90e',1,'_spi_master_handle::userData()'],['../a00038.html#ad12378e2c097e3b73171f1935e8b68c7',1,'_spi_dma_handle::userData()'],['../a00041.html#a69ce1fdb7a2f60c0ecc94c4d1b2ed6ff',1,'_uart_handle::userData()'],['../a00042.html#ae170e623aab9c539b8793b2ffaa3b1c7',1,'_uart_dma_handle::userData()'],['../a00043.html#a234d609cec5123bf352a4fa3f806375b',1,'_uart_edma_handle::userData()'],['../a00029.html#a527a36e97ff5a7c65b27adbadd3ea18f',1,'sdmmchost_detect_card_t::userData()'],['../a00029.html#ae560f7aa2036cba9a2fd4ae7916e864a',1,'sdmmchost_card_int_t::userData()'],['../a00023.html#a5e90e1dafbe97f2ea5187530ee04af0a',1,'notifier_handle_t::userData()']]],
  ['userfunction',['userFunction',['../a00023.html#a4f6589fa386e93c40c52ed292da4f47f',1,'notifier_handle_t']]],
  ['userpartitionblocks',['userPartitionBlocks',['../a00022.html#aaf035e6ccfee3bd92ef690d2aff5254d',1,'mmc_card_t']]],
  ['userwp',['userWP',['../a00021.html#a9cb3b77ae5002d9d9eb737285206e19f',1,'mmc_extended_csd_t']]],
  ['usrparam',['usrParam',['../a00022.html#a1ade0131fbf0b690de0e58eeb7d041bf',1,'mmc_card_t::usrParam()'],['../a00027.html#a8576692c965aa1ae68a5acac9fe11e2d',1,'sd_card_t::usrParam()'],['../a00028.html#ga34d31d24342037e0e54bf64f34f69e1a',1,'_sdio_card::usrParam()']]]
];
