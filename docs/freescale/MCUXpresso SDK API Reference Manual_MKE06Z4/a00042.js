var a00042 =
[
    [ "uart_dma_handle_t", "a00042.html#a00143", [
      [ "base", "a00042.html#ac5bbb81df9c93c195d060b5d7d418c60", null ],
      [ "callback", "a00042.html#ab692a5606001d4672b51b319443bc90c", null ],
      [ "userData", "a00042.html#ae170e623aab9c539b8793b2ffaa3b1c7", null ],
      [ "rxDataSizeAll", "a00042.html#a87337162e7c49bab4e16587cbe47088a", null ],
      [ "txDataSizeAll", "a00042.html#ac258dbe5efc21160ef52285418187f33", null ],
      [ "txDmaHandle", "a00042.html#af5626cb851f69637fb71eb28da9dae98", null ],
      [ "rxDmaHandle", "a00042.html#a4bd3fe36e857c34fce6556ad7e031c1c", null ],
      [ "txState", "a00042.html#a739848198af89237f9506611fb787c78", null ],
      [ "rxState", "a00042.html#a1545b5d899ea275d3043c975983215f7", null ]
    ] ],
    [ "FSL_UART_DMA_DRIVER_VERSION", "a00042.html#ga92e49e50a854c77a0a7d75fa4780d592", null ],
    [ "uart_dma_transfer_callback_t", "a00042.html#gad4fe061c9b2450d48570109b2e2cd654", null ],
    [ "UART_TransferCreateHandleDMA", "a00042.html#gab2070d0ced69f5bc55f9a065616dbeb3", null ],
    [ "UART_TransferSendDMA", "a00042.html#ga4a7df0f05dcbc400c58444297e9bcb78", null ],
    [ "UART_TransferReceiveDMA", "a00042.html#gae866ac13d35c784fcb932ea5e3a7eb24", null ],
    [ "UART_TransferAbortSendDMA", "a00042.html#ga274eeaf8efa8dd67b314257d25049d53", null ],
    [ "UART_TransferAbortReceiveDMA", "a00042.html#gaab860621a623403253c85079ae0d0544", null ],
    [ "UART_TransferGetSendCountDMA", "a00042.html#gab6d8846749280ae49e8f4e67bb57604a", null ],
    [ "UART_TransferGetReceiveCountDMA", "a00042.html#ga6405510d2411941043b2d645f131984a", null ]
];