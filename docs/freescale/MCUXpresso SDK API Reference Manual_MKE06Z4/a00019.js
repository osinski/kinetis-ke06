var a00019 =
[
    [ "kbi_config_t", "a00019.html#a00173", [
      [ "pinsEnabled", "a00019.html#a5aed87f3f64c90d32861228d402bf394", null ],
      [ "pinsEdge", "a00019.html#ae4584c97cd409435af9ba642ca9f50ce", null ],
      [ "mode", "a00019.html#a4dfa0a64d6550817eaa7907ed93923d8", null ]
    ] ],
    [ "FSL_KBI_DRIVER_VERSION", "a00019.html#ga909ec244d50a48ee89692d7ca26d44ac", null ],
    [ "kbi_detect_mode_t", "a00019.html#ga78132778db62901ad0d9d977ec66f107", [
      [ "kKBI_EdgesDetect", "a00019.html#gga78132778db62901ad0d9d977ec66f107a6effbeb1ee9f942505676946788ec4b2", null ],
      [ "kKBI_EdgesLevelDetect", "a00019.html#gga78132778db62901ad0d9d977ec66f107adce70e2d228d3aa82bccf16ad40c2bd1", null ]
    ] ],
    [ "KBI_Init", "a00019.html#gacee9dc478db3c00a4c56caaea3693ac2", null ],
    [ "KBI_Deinit", "a00019.html#gab54fcae5ecca65433c98581e198fda5b", null ],
    [ "KBI_EnableInterrupts", "a00019.html#ga742878b11e138465d384291f778852e2", null ],
    [ "KBI_DisableInterrupts", "a00019.html#ga4d71d8ce3224ce24f8f199621fa12f25", null ],
    [ "KBI_IsInterruptRequestDetected", "a00019.html#ga8180c69bfb529d844a71f263c6efa278", null ],
    [ "KBI_ClearInterruptFlag", "a00019.html#ga8c6779102ded4f55df48f79c123c9586", null ],
    [ "KBI_GetSourcePinStatus", "a00019.html#gae6b201ca3572d25df8a5dc95bcf3e7df", null ]
];