var a00044 =
[
    [ "uart_rtos_config_t", "a00044.html#a00221", [
      [ "base", "a00044.html#af93759d3503ab560aef347c02e5f42dd", null ],
      [ "srcclk", "a00044.html#aaa9ea3cb62d50a49b907b1baddbeeaa0", null ],
      [ "baudrate", "a00044.html#a3a516fb385a59c89720a3dead326df78", null ],
      [ "parity", "a00044.html#a8d8809aff183104892d30cc0734679bd", null ],
      [ "stopbits", "a00044.html#a2afb208100058edfc05aa161e555483f", null ],
      [ "buffer", "a00044.html#ad05b4abce6a95baa7ba35eaa57569cfe", null ],
      [ "buffer_size", "a00044.html#a7b7d6d667f6e06c720f506a07869e14d", null ]
    ] ],
    [ "FSL_UART_FREERTOS_DRIVER_VERSION", "a00044.html#ga0023d77491809dff862358266574febb", null ],
    [ "UART_RTOS_Init", "a00044.html#ga1ca9d0f6b8d4d5fc3e64c9a57d7ada7d", null ],
    [ "UART_RTOS_Deinit", "a00044.html#gab7ac281cb85e1d290c8f25c83a6facfc", null ],
    [ "UART_RTOS_Send", "a00044.html#gada4115d237b9517f2587a054dbdc442a", null ],
    [ "UART_RTOS_Receive", "a00044.html#gaa8adcb11232b565985f3f5961815a00c", null ]
];