var a00029 =
[
    [ "sdmmchost_detect_card_t", "a00029.html#a00201", [
      [ "cdType", "a00029.html#acf639c21ecedefd38834ffe896528f3c", null ],
      [ "cdTimeOut_ms", "a00029.html#a3834ad44ec29fe8e6bb55abb122b833e", null ],
      [ "cardInserted", "a00029.html#a57c6b2cce6097412ab32e616cac47a6c", null ],
      [ "cardRemoved", "a00029.html#afea184ea290c404f6f2928ef1053a281", null ],
      [ "userData", "a00029.html#a527a36e97ff5a7c65b27adbadd3ea18f", null ]
    ] ],
    [ "sdmmchost_pwr_card_t", "a00029.html#a00202", [
      [ "powerOn", "a00029.html#a5f92780db6b7b671b2e278fb37f086a3", null ],
      [ "powerOnDelay_ms", "a00029.html#a2c042fa348f296d9a00afb5ae8a5f855", null ],
      [ "powerOff", "a00029.html#ac34f526578869ddf61e0c912d583476a", null ],
      [ "powerOffDelay_ms", "a00029.html#aec08e8db9fb47c742556038e650521db", null ]
    ] ],
    [ "sdmmchost_card_int_t", "a00029.html#a00199", [
      [ "userData", "a00029.html#ae560f7aa2036cba9a2fd4ae7916e864a", null ],
      [ "cardInterrupt", "a00029.html#ada6500c315f9fb52f7c8e49cfa263464", null ]
    ] ],
    [ "sdmmchost_card_switch_voltage_func_t", "a00029.html#a00200", [
      [ "cardSignalLine1V8", "a00029.html#a01e35b84e668f7d8a6c686ea0b8e8de2", null ],
      [ "cardSignalLine3V3", "a00029.html#a8efea0c1fdb5885c6fa73b00fd57d11a", null ]
    ] ],
    [ "sdmmhostcard_usr_param_t", "a00029.html#a00203", [
      [ "cd", "a00029.html#a40c671d95f63368a373fbbf73b949cfe", null ],
      [ "pwr", "a00029.html#a0212601045d8d9c2c9894de00e47d361", null ],
      [ "cardInt", "a00029.html#a0186e4a00a22c7a4324f310776d62770", null ],
      [ "cardVoltage", "a00029.html#a68cb5748d1411a9b78904a022c6c320c", null ]
    ] ],
    [ "FSL_SDMMC_HOST_ADAPTER_VERSION", "a00029.html#gab7f416230cd008bf4453aca9e4ae6623", null ],
    [ "SDMMCHOST_NOT_SUPPORT", "a00029.html#ga90c279e22c7d275339049590375fb0c4", null ],
    [ "SDMMCHOST_SUPPORT", "a00029.html#ga35feac92e560e57dbdc83b7dcaca61e8", null ],
    [ "sdmmchost_cd_callback_t", "a00029.html#gac00db67cc6717a3674a4f970abd7b152", null ],
    [ "sdmmchost_pwr_t", "a00029.html#gae6c9fa0b6cb785c1aeb2aa4a0204de6a", null ],
    [ "sdmmchost_card_int_callback_t", "a00029.html#ga916ca0ca932356eb7ace844b89e0f712", null ],
    [ "sdmmchost_card_switch_voltage_t", "a00029.html#ga590009d7132af60aa2779c8b95577da5", null ],
    [ "sdcard_usr_param_t", "a00029.html#gad07b0dfe192164db338da126ec31d3d0", null ],
    [ "_sdmmchost_endian_mode", "a00029.html#gaf0790af3dd4f5150d70749a16b8a3273", [
      [ "kSDMMCHOST_EndianModeBig", "a00029.html#ggaf0790af3dd4f5150d70749a16b8a3273a4ed3ac858d9b99252a49ca7a297d2dd0", null ],
      [ "kSDMMCHOST_EndianModeHalfWordBig", "a00029.html#ggaf0790af3dd4f5150d70749a16b8a3273a2394f707bc8124fa714e61e0d9748264", null ],
      [ "kSDMMCHOST_EndianModeLittle", "a00029.html#ggaf0790af3dd4f5150d70749a16b8a3273accb3b8b837fb20c01c79a99364ca0138", null ]
    ] ],
    [ "sdmmchost_detect_card_type_t", "a00029.html#ga8bdb8b2058dd1ea26fac6384951c8f60", [
      [ "kSDMMCHOST_DetectCardByGpioCD", "a00029.html#gga8bdb8b2058dd1ea26fac6384951c8f60a5ea36fe892d611a1d1932f9af3ba8e49", null ],
      [ "kSDMMCHOST_DetectCardByHostCD", "a00029.html#gga8bdb8b2058dd1ea26fac6384951c8f60a71f13f53d389f55d0988155d0ab851d5", null ],
      [ "kSDMMCHOST_DetectCardByHostDATA3", "a00029.html#gga8bdb8b2058dd1ea26fac6384951c8f60a0730044ab57bc5c6de34eb5b00e8ed0f", null ]
    ] ],
    [ "SDMMCHOST_NotSupport", "a00029.html#ga288015c25bb3b70d906c12df682a5c3c", null ],
    [ "SDMMCHOST_WaitCardDetectStatus", "a00029.html#ga6a91d7919263d37e71f148ce4f8cba7c", null ],
    [ "SDMMCHOST_IsCardPresent", "a00029.html#gadc5e291de0889b4cde065caaab70433b", null ],
    [ "SDMMCHOST_Init", "a00029.html#ga4b98bad7441e285f4eb84118a70fe4f0", null ],
    [ "SDMMCHOST_Reset", "a00029.html#gaa009778ed59465c5d3951a4e60967ebe", null ],
    [ "SDMMCHOST_ErrorRecovery", "a00029.html#ga461682526c594e4f1e31526b0a98964c", null ],
    [ "SDMMCHOST_Deinit", "a00029.html#ga9ab8acd3a108c63c707157c530953783", null ],
    [ "SDMMCHOST_PowerOffCard", "a00029.html#ga78baa10b1f20d2a398b1533baad9bf95", null ],
    [ "SDMMCHOST_PowerOnCard", "a00029.html#gaa256b5bc8950722ab45172f207cc11d5", null ],
    [ "SDMMCHOST_Delay", "a00029.html#ga8002480a4e88e1b1d644f89f3f3d5c0b", null ],
    [ "SDMMCHOST_ReceiveTuningBlock", "a00029.html#ga8fa61a656bddd124ad8e615140665b63", null ]
];