var a00015 =
[
    [ "i2c_master_dma_handle_t", "a00015.html#a00136", [
      [ "transfer", "a00015.html#a2a42954d3ed3c3138a2e0470d10d4485", null ],
      [ "transferSize", "a00015.html#a2b7048fa97083cb5e44c75755d1331a6", null ],
      [ "state", "a00015.html#acb92e84abaa14c90df432c0a9e2584d4", null ],
      [ "dmaHandle", "a00015.html#a3165a424be14bb2066675341559a538b", null ],
      [ "completionCallback", "a00015.html#a4d36b82522c60896cae7d86d187ecaf4", null ],
      [ "userData", "a00015.html#a9d8fa45695a25b132f465762176f7c88", null ]
    ] ],
    [ "FSL_I2C_DMA_DRIVER_VERSION", "a00015.html#ga2abc151af9c3437ca8da73905887d962", null ],
    [ "i2c_master_dma_transfer_callback_t", "a00015.html#gac464cb9c433f4447f0e3f0f47722793a", null ],
    [ "I2C_MasterTransferCreateHandleDMA", "a00015.html#ga83d8463e42b12e7b797a3f928f51b3cb", null ],
    [ "I2C_MasterTransferDMA", "a00015.html#ga8643ea43510b8d97b94db05766444cd2", null ],
    [ "I2C_MasterTransferGetCountDMA", "a00015.html#ga4bf5fbe48e6776cbd6ef017b6785f745", null ],
    [ "I2C_MasterTransferAbortDMA", "a00015.html#ga209877e924b049c835adc6bfb8690f5a", null ]
];