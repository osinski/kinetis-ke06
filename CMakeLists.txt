cmake_minimum_required(VERSION 3.13)
project(FRDM_KE06
        LANGUAGES ASM C CXX
        VERSION 1.0
        DESCRIPTION "Testing the FRDM_KE06 board"
)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)
set(CMAKE_C_STANDARD 11)
set(CMAKE_C_STANDARD_REQUIRED True)

set(FILE_ELF ${CMAKE_PROJECT_NAME}.elf)
set(FILE_HEX ${CMAKE_PROJECT_NAME}.hex)

#####   OPTIONS     ############################################################
option(GENERATE_MAP "Generate *.map file" OFF)
option(BUILD_DOCS "Build documentation" OFF)

option(FLASH_AFTER_BUILD "Flash MCU automatically after build" OFF)
option(DEBUG_AFTER_BUILD "Flash MCU and run GDB automatically after build" OFF)
################################################################################

#####   FLASH AND DEBUG UTILS   ################################################
set(JLINK_MCU_NAME "MKE06Z128xxx4")
set(JLINK_INTERFACE "SWD")
set(JLINK_IF_SPEED "4000")

configure_file("${PROJECT_SOURCE_DIR}/scripts/flash.sh.in"
                "${PROJECT_SOURCE_DIR}/scripts/flash.sh")
configure_file("${PROJECT_SOURCE_DIR}/scripts/flash.jlink.in"
                "${PROJECT_SOURCE_DIR}/scripts/flash.jlink")

configure_file("${PROJECT_SOURCE_DIR}/scripts/erase.sh.in"
                "${PROJECT_SOURCE_DIR}/scripts/erase.sh")
configure_file("${PROJECT_SOURCE_DIR}/scripts/erase.jlink.in"
                "${PROJECT_SOURCE_DIR}/scripts/erase.jlink")

configure_file("${PROJECT_SOURCE_DIR}/scripts/softreset.sh.in"
                "${PROJECT_SOURCE_DIR}/scripts/softreset.sh")
configure_file("${PROJECT_SOURCE_DIR}/scripts/softreset.jlink.in"
                "${PROJECT_SOURCE_DIR}/scripts/softreset.jlink")

configure_file("${PROJECT_SOURCE_DIR}/scripts/debug.sh.in"
                "${PROJECT_SOURCE_DIR}/scripts/debug.sh")
################################################################################

# Add linker script to link options
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS}\
                            -T${PROJECT_SOURCE_DIR}/gcc/MKE06Z128xxx4_flash.ld")

if(BUILD_DOCS)
    add_subdirectory(docs)
    # TODO
endif()

if(GENERATE_MAP)
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS}
                                 -Wl,-Map=${CMAKE_PROJECT_NAME}.map
                                 -Wl,--cref"
    )
endif()

add_subdirectory(libs)

add_executable(${FILE_ELF} ${SOURCES}
                           ${CMAKE_CURRENT_SOURCE_DIR}/gcc/startup_MKE06Z4.S)

add_subdirectory(src)

target_link_libraries(${FILE_ELF} fsl cmsis)

target_compile_options(${FILE_ELF}
    PRIVATE
        $<$<COMPILE_LANGUAGE:NOT ASM>:" -Wall -Wextra">
)

#####   POST BUILD  ############################################################
add_custom_command(TARGET ${FILE_ELF} POST_BUILD
                    COMMENT "Creating .hex file.."
                    COMMAND ${OBJCOPY} -O ihex ${FILE_ELF} ${FILE_HEX}
)

add_custom_command(TARGET ${FILE_ELF} POST_BUILD
                    COMMENT "Calculating size.."
                    COMMAND ${OBJSIZE} ${FILE_ELF}
)


if(FLASH_AFTER_BUILD)
    set(FLASH_ALL_OPTION ALL)
else()
    set(FLASH_ALL_OPTION)
endif()

if(DEBUG_AFTER_BUILD)
    set(DEBUG_ALL_OPTION ALL)
else()
    set(DEBUG_ALL_OPTION)
endif()

add_custom_target(flasher ${FLASH_ALL_OPTION}
        COMMENT "Flashing MCU.."
        COMMAND ${PROJECT_SOURCE_DIR}/scripts/flash.sh
        DEPENDS ${FILE_ELF}
)

add_custom_target(debugger ${DEBUG_ALL_OPTION}
        COMMENT "Debugging MCU.."
        COMMAND ${PROJECT_SOURCE_DIR}/scripts/debug.sh
        DEPENDS ${FILE_ELF}
)

add_custom_target(softreset
        COMMENT "Resetting MCU.."
        COMMAND ${PROJECT_SOURCE_DIR}/scripts/softreset.sh
)

add_custom_target(erase
        COMMENT "Erasing MCU.."
        COMMAND ${PROJECT_SOURCE_DIR}/scripts/erase.sh
)

################################################################################
