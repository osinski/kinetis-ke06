cmake_minimum_required(VERSION 3.13)

add_library(cmsis INTERFACE)

target_sources(cmsis 
    INTERFACE
        core_cm0plus.h
        cmsis_compiler.h
        cmsis_gcc.h
        cmsis_version.h
        arm_common_tables.h
        arm_const_structs.h
        arm_math.h
)

target_include_directories(cmsis INTERFACE ${CMAKE_CURRENT_LIST_DIR})
